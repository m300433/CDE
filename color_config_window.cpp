
#include "color_config_window.h"
#include "colorScheme.h"
#include "colorSchemeMap.h"
#include "fieldStats.h"
#include <iostream>
#include <map>
#include <vector>

ColorConfigWindow::ColorConfigWindow(ColorSchemeMap &p_color_schemes, FieldStats &p_field_stats,
                                     QWidget *p_parent)
    : m_color_schemes(p_color_schemes), m_field_stats(p_field_stats), QWidget(p_parent) {
    m_current_min = p_field_stats.min;
    m_current_max = p_field_stats.max;
    create_layout();
}

void ColorConfigWindow::create_layout() {

    m_mainLayout = new QHBoxLayout(this);
    {
        m_colorPlotWidget = new QWidget(this);
        {
            m_colorPlotWidget->setMinimumSize(QSize(600, 500));

            m_colorPlot = new QCustomPlot(m_colorPlotWidget);
            {

                m_gradient = new QCPColorGradient();
                m_colorScale = new QCPColorScale(m_colorPlot);
                {
                    m_colorScale->setType(QCPAxis::atRight);
                    m_colorScale->axis()->setLabel("Gridpoint value");
                }
                m_colorPlot->plotLayout()->addElement(0, 1, m_colorScale);

                m_colorMap = new QCPColorMap(m_colorPlot->xAxis, m_colorPlot->yAxis);
                {
                    m_colorMap->setInterpolate(false);
                    m_colorMap->setColorScale(m_colorScale);
                }
                m_colorPlot->setInteractions(QCP::iRangeZoom);
                m_colorPlot->yAxis->setLabel("colorIndex");
                m_colorPlot->xAxis->setLabel("data");
                m_colorPlot->setMinimumSize(QSize(600, 500));
                m_colorPlot->addGraph();
            }
        }
        m_colorManipulationLayout = new QVBoxLayout();
        {
            m_colorSchemeChooser = new QComboBox();
            m_colorSliderLayout = new QHBoxLayout();
            {
                m_upperBoundsSlider = new QSlider(Qt::Vertical, this);
                m_lowerBoundsSlider = new QSlider(Qt::Vertical, this);
                m_colorSliderLayout->addWidget(m_upperBoundsSlider);
                m_colorSliderLayout->addWidget(m_lowerBoundsSlider);
            }
            m_colorManipulationLayout->addWidget(m_colorSchemeChooser);
            m_colorManipulationLayout->addLayout(m_colorSliderLayout);
        }
        m_mainLayout->addWidget(m_colorPlotWidget);
        m_mainLayout->addLayout(m_colorManipulationLayout);
    }
    this->setWindowTitle(tr("&Color configuration"));
    this->setLayout(m_mainLayout);
}

void ColorConfigWindow::update_slider() {
}
void ColorConfigWindow::update_plot() {
    std::vector<double> &current_ranges = m_color_schemes.current_scheme_ranges();
    m_y_axis_values = QVector<double>(m_color_schemes.current_scheme_size());
    for (unsigned int i = 0; i < m_color_schemes.current_scheme_size(); i++) {
        m_y_axis_values[i] = i;
    }
    m_colorPlot->yAxis->setRange(0, m_color_schemes.current_scheme_size());
    m_colorPlot->xAxis->setRange(QCPRange(current_ranges[0],
                                          current_ranges[current_ranges.size() - 2]));
    auto last_for_graph_relevant_range_entry = current_ranges.begin() + current_ranges.size() - 2;
    m_colorPlot->graph(0)->setData(
        QVector<double>::fromStdVector(std::vector<double>(current_ranges.begin(), last_for_graph_relevant_range_entry)), m_y_axis_values);
    m_colorPlot->replot();
}
void ColorConfigWindow::update_color_map() {

    std::vector<double> &current_ranges = m_color_schemes.current_scheme_ranges();
    std::vector<int> color;
    QCPColorGradient color_gradient;
    color_gradient.clearColorStops();
    color_gradient.setLevelCount(m_color_schemes.current_scheme_size());
    unsigned int size = m_color_schemes.current_scheme_size();//current_ranges[current_ranges.size() -  2] - current_ranges[0];
    m_colorMap->data()->setSize(size, 1);
    m_colorMap->data()->setRange(QCPRange(current_ranges[0], current_ranges[current_ranges.size() -  2]),
                                 QCPRange(0, m_color_schemes.current_scheme_size()));
    for (unsigned long i = 0; i < m_color_schemes.current_scheme_size(); i++) {

        color = m_color_schemes.current_scheme_colors()[i];
        double stop =
            (current_ranges[i] - current_ranges[0]) / (current_ranges[current_ranges.size() - 2] - current_ranges[0]);
        color_gradient.setColorStopAt(stop, QColor(color[0], color[1], color[2]));
        m_colorMap->data()->setCell(i, 0, current_ranges[i]);
    }
    m_colorMap->rescaleDataRange();
    m_colorMap->rescaleAxes();

    m_colorMap->setGradient(color_gradient);
}

void ColorConfigWindow::update_gradient() {
    /*
    m_gradient->setLevelCount(m_color_schemes.current_scheme_size());
    for (unsigned int i = 0; i < m_color_schemes.current_scheme_size(); i++) {
        std::vector<int> color = m_color_schemes.current_scheme_colors()[i];
        m_gradient->clearColorStops();
        m_gradient->setColorStopAt((double)i/(double)m_color_schemes.current_scheme_size(),
                                   QColor(color[0], color[1], color[2]));
    }
    m_gradient->setPeriodic(true);
    */
}
void ColorConfigWindow::update_color_scheme_chooser() {
}
float ColorConfigWindow::get_current_range() {
    return m_current_max - m_current_min;
}
void ColorConfigWindow::update() {
    m_current_min = m_field_stats.min;
    m_current_max = m_field_stats.max;
    update_gradient();
    update_color_map();
    update_color_scheme_chooser();
    update_slider();
    update_plot();
}
