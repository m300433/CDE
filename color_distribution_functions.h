#ifndef COLOR_DISTRIBUTION_FUNCTIONS_H
#define COLOR_DISTRIBUTION_FUNCTIONS_H
#include "colorScheme.h"
#include "fieldStats.h"
#include <vector>

std::vector<double> create_linear_distribution(unsigned long p_cnt_colors, FieldStats p_data_limts);
std::vector<double> create_splitted_linear_distribution(unsigned long p_cnt_colors,
                                                        FieldStats p_data_limts);

#endif
