#include "ConfigDialog.h"
#include "MainWindow.h"
#include "ColorButton.h"

#include <QLocale>
#include <QPushButton>
#include <QLabel>
#include <QGridLayout>
#include <QGroupBox>
#include <QFont>
#include <QFontDialog>
#include <QColorDialog>
#include <QTabWidget>
#include <QStackedWidget>
#include <QWidget>
#include <QComboBox>
#include <QSpinBox>
#include <QRadioButton>
#include <QStyleFactory>
#include <QRegExp>
#include <QMessageBox>
#include <QTranslator>
#include <QApplication>
#include <QDir>
#include <QPixmap>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QListWidget>
#include <QFontMetrics>


ConfigDialog::ConfigDialog( QWidget* parent, Qt::WindowFlags fl )
    : QDialog( parent, fl )
{
  // get current values from main window
  MainWindow *mainwindow = (MainWindow *)parentWidget();

  // create the GUI
  generalDialog = new QStackedWidget();
  itemsList = new QListWidget();
  itemsList->setSpacing(10);
  itemsList->setAlternatingRowColors( true );

  initAppPage();

  generalDialog->addWidget(appTabWidget);

  QVBoxLayout * rightLayout = new QVBoxLayout();
  lblPageHeader = new QLabel();
  QFont fnt = this->font();
  fnt.setPointSize(fnt.pointSize() + 3);
  fnt.setBold(true);
  lblPageHeader->setFont(fnt);
  lblPageHeader->setFrameStyle(QFrame::StyledPanel | QFrame::Sunken);

  QPalette pal = lblPageHeader->palette();
  pal.setColor( QPalette::Window, mainwindow->panelsColor );
  lblPageHeader->setPalette(pal);
  lblPageHeader->setAutoFillBackground( true );

  rightLayout->setSpacing(10);
  rightLayout->addWidget( lblPageHeader );
  rightLayout->addWidget( generalDialog );

  QHBoxLayout * topLayout = new QHBoxLayout();
  topLayout->setSpacing(5);
  topLayout->setMargin(5);
  topLayout->addWidget( itemsList );
  topLayout->addLayout( rightLayout );

  QHBoxLayout * bottomButtons = new QHBoxLayout();
  bottomButtons->addStretch();
  buttonApply = new QPushButton();
  buttonApply->setAutoDefault( true );
  bottomButtons->addWidget( buttonApply );

  buttonOk = new QPushButton();
  buttonOk->setAutoDefault( true );
  buttonOk->setDefault( true );
  bottomButtons->addWidget( buttonOk );

  buttonCancel = new QPushButton();
  buttonCancel->setAutoDefault( true );
  bottomButtons->addWidget( buttonCancel );

  QVBoxLayout * mainLayout = new QVBoxLayout( this );
  mainLayout->addLayout(topLayout);
  mainLayout->addLayout(bottomButtons);

  languageChange();

  // signals and slots connections
  connect( itemsList, SIGNAL(currentRowChanged(int)), this, SLOT(setCurrentPage(int)));
  connect( buttonOk, SIGNAL( clicked() ), this, SLOT( accept() ) );
  connect( buttonApply, SIGNAL( clicked() ), this, SLOT( apply() ) );
  connect( buttonCancel, SIGNAL( clicked() ), this, SLOT( reject() ) );

  setCurrentPage(0);
}


void ConfigDialog::setCurrentPage(int index)
{
  generalDialog->setCurrentIndex(index);

  if ( itemsList->currentItem() )
    lblPageHeader->setText(itemsList->currentItem()->text());
}


/*
void ConfigDialog::showFrameWidth(bool ok)
{
  if (!ok)
    {
      boxFrameWidth->hide();
      labelFrameWidth->hide();
    }
  else
    {
      boxFrameWidth->show();
      labelFrameWidth->show();
    }
}
*/

void ConfigDialog::initAppPage()
{
  MainWindow *mainwindow = (MainWindow *)parentWidget();

  appTabWidget = new QTabWidget(generalDialog);

  application = new QWidget();
  QVBoxLayout * applicationLayout = new QVBoxLayout( application );
  QGroupBox * groupBoxApp = new QGroupBox();
  applicationLayout->addWidget(groupBoxApp);
  QGridLayout * topBoxLayout = new QGridLayout( groupBoxApp );

  lblStyle = new QLabel();
  topBoxLayout->addWidget( lblStyle, 1, 0 );
  boxStyle = new QComboBox();
  topBoxLayout->addWidget( boxStyle, 1, 1 );
  QStringList styles = QStyleFactory::keys();
  styles.sort();
  boxStyle->addItems(styles);
  boxStyle->setCurrentIndex(boxStyle->findText(mainwindow->appStyle,Qt::MatchWildcard));

  topBoxLayout->setRowStretch( 6, 1 );

  appTabWidget->addTab( application, QString() );
  
  appColors = new QWidget();
  QVBoxLayout * appColorsLayout = new QVBoxLayout( appColors );
  QGroupBox * groupBoxAppCol = new QGroupBox();
  appColorsLayout->addWidget( groupBoxAppCol );
  QGridLayout * colorsBoxLayout = new QGridLayout( groupBoxAppCol );

  lblWorkspace = new QLabel();
  colorsBoxLayout->addWidget( lblWorkspace, 0, 0 );
  btnWorkspace = new ColorButton();
  btnWorkspace->setColor(mainwindow->workspaceColor);
  colorsBoxLayout->addWidget( btnWorkspace, 0, 1 );

  lblPanels = new QLabel();
  colorsBoxLayout->addWidget( lblPanels, 1, 0 );
  btnPanels = new ColorButton();
  colorsBoxLayout->addWidget( btnPanels, 1, 1 );
  btnPanels->setColor(mainwindow->panelsColor);

  colorsBoxLayout->setRowStretch( 2, 1 );

  appTabWidget->addTab( appColors, QString() );

  connect( btnWorkspace, SIGNAL( clicked() ), this, SLOT( pickWorkspaceColor() ) );
  connect( btnPanels, SIGNAL( clicked() ), this, SLOT( pickPanelsColor() ) );
}


void ConfigDialog::languageChange()
{
  setWindowTitle( tr( "Preferences" ) );

  // pages list
  itemsList->clear();
  itemsList->addItem( tr( "General" ) );
  itemsList->setCurrentRow(0);
  itemsList->item(0)->setIcon(QIcon(QPixmap(":/images/general.xpm")));
  itemsList->setIconSize(QSize(32,32));
  // calculate a sensible width for the items list
  // (default QListWidget size is 256 which looks too big)
  QFontMetrics fm(itemsList->font());
  int width = 32,i;
  for(i=0 ; i<itemsList->count() ; i++)
    if( fm.width(itemsList->item(i)->text()) > width)
      width = fm.width(itemsList->item(i)->text());
  itemsList->setMaximumWidth( itemsList->iconSize().width() + width + 50 );
  // resize the list to the maximum width
  itemsList->resize(itemsList->maximumWidth(),itemsList->height());

  //boxResize->setText(tr("Do not &resize layers when window size changes"));

  buttonOk->setText( tr( "&OK" ) );
  buttonCancel->setText( tr( "&Cancel" ) );
  buttonApply->setText( tr( "&Apply" ) );

  //application page
  appTabWidget->setTabText(appTabWidget->indexOf(application), tr("Application"));
  appTabWidget->setTabText(appTabWidget->indexOf(appColors), tr("Colors"));

  //lblLanguage->setText(tr("Language"));
  lblStyle->setText(tr("Style"));

  lblWorkspace->setText(tr("Workspace"));
  lblPanels->setText(tr("Panels"));
}


void ConfigDialog::accept()
{
  apply();
  close();
}


void ConfigDialog::apply()
{
  MainWindow *mainwindow = (MainWindow *)parentWidget();

  if ( !mainwindow ) return;

  // general page: application tab
  mainwindow->changeAppStyle(boxStyle->currentText());	

  mainwindow->setAppColors(btnWorkspace->color(), btnPanels->color());
}


void ConfigDialog::pickPanelsColor()
{
  QColor c = QColorDialog::getColor(btnPanels->color(), this);
  if ( !c.isValid() || c == btnPanels->color())
    return;

  btnPanels->setColor(c);
}


void ConfigDialog::pickWorkspaceColor()
{
  QColor c = QColorDialog::getColor(btnWorkspace->color(), this);
  if ( !c.isValid() || c == btnWorkspace->color())
    return;

  btnWorkspace->setColor(c);
}
