DONE:
- show value in edit window (3 DONE
    if multiple{ show 0}; DONE
- old color values for distribution or cnt_clr/2 foreach side (2nd way done)
- outcomment movement (2 (NE NEED ARROWS WORK => DONE)
- SAVE(DONE)
-grid ID(DONE)
-(lon lat reinfolge (kennzeichnen))(CANCELED)

TODO:

- drucken (P5) farbscale muss mit!
- colorschemes (P3) (HALF DONE)
- file_name (P2)
- rename View -> MainFrame
- lon/lat jump (HALTED)(HALF DONE)
- refactor: edit and lon lat into own class
- refactor: move into GraphicsView/View (P1){
    left mouse select on release (
    if moved => no select(P6)
    main-window farbscala (4)
    multiple windows (LAST PRIO - 1)
    line/box selection + modifier (P7)
    rahmen kasten raus, dafuer kasyten einfaerben. (P8)
    }
- find and remove mem leaks (LAST PRIO)



Questions:
    -workflow: multiple windows vs multiple tabs in 1 window?
    A:  file = window, var = tab, move for tabs (toggable)
    -shorcuts ala vim vs used to usage?((olo
    A: OK 
    -print to pdf or to printer?
    A: pdf

Problems if:
    if multiple windows:
        - more complex to sync movement and changes.
        - data storage => where is the loaded data stored (file manager class?)
        - multiple files -> ram usage?
        - what kind of files are used (pre selected?) (levels etc)
