#ifndef FIELD_STATS_H
#define FIELD_STATS_H

struct FieldStats {
    FieldStats() : min(-7000.0), max(2000.0) {
        range = max - min;
    }
    int gridsize;
    double min;
    double max;
    int nmiss;
    double missval;
    double mean;
    double var;
    double range;
};

#endif
