#ifndef COLOR_SCHEME_MAP_H
#define COLOR_SCHEME_MAP_H

#include "cereal/archives/xml.hpp"
#include "colorScheme.h"
#include <QColor>
#include <map>
#include <memory>
#include <vector>

class ColorSchemeMap {
  private:
    std::map<std::string, ColorScheme> m_color_map;
    std::string m_current_color_scheme_name;

  public:
    ColorSchemeMap();
    ColorScheme &current_scheme();
    std::string current_scheme_name();
    std::vector<double> &current_scheme_ranges();
    unsigned long current_scheme_size();

    void save_schemes();
    void load_schemes();
    void set_color_scheme(std::string p_color_scheme_name);
    void add_scheme(std::string, std::vector<QColor>);
    std::vector<std::vector<int>> &current_scheme_colors();
};    
#endif
