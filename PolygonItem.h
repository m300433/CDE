#ifndef POLYGONITEM_H
#define POLYGONITEM_H

#include <QGraphicsPolygonItem>
#include <QtWidgets>

enum e_poly_data  {ID = 0,LON = 1, LAT = 2, HEIGHT = 3};
class PolygonItem : public QObject, public QGraphicsPolygonItem {
    Q_OBJECT
  public:
    PolygonItem(QPolygon &polygon, unsigned p_id, double p_value, double p_lon, double p_lan);
    void cancel_selection();
    void set_value(double p_value);
    void set_color(QColor p_color);
  signals:
    void poly_item_selection_canceled(PolygonItem *p_item);
    void poly_item_all_selections_canceled();
    void poly_item_was_selected(PolygonItem *p_item);
    void poly_context_menu_open(PolygonItem *p_item);

  protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event);

  private:
    bool m_selected;
};

#endif
