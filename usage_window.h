#ifndef USAGE_WINDOW_HPP
#define USAGE_WINDOW_HPP

#include <QWidget>

#include <QHBoxLayout>
#include <QLabel>
#include <iostream>

class UsageWindow : public QWidget {
  public:
    UsageWindow();

  private:
    QHBoxLayout *layout; // TODO: rename
    QLabel *m_usage;
};

#endif
