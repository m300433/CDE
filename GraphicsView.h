#ifndef GRAPHICSVIEW_H
#define GRAPHICSVIEW_H

#include <QtWidgets/QGraphicsView>
#include <QRubberBand>


class GraphicsView : public QGraphicsView
{
    Q_OBJECT

public:
    GraphicsView();
    void scaleView(qreal scaleFactor);
    //   void translateView();
    void resetView();

protected:
    void wheelEvent(QWheelEvent *event);
    /*
    void mouseMoveEvent(QMouseEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    */
    void xresizeEvent(QResizeEvent *event);
    //void drawBackground(QPainter *painter, const QRectF &rect);
    void drawForeground(QPainter *painter, const QRectF &rect);
private:
    double m_current_scale;
    QPoint m_selection_rectangle_orgigin;
    QRubberBand *m_selection_rectangle;
    bool m_zoom_rectangle_active;
};

#endif
