#include "colorScheme.h"
#include "color_distribution_functions.h"
#include "fieldStats.h"
#include <QColor>
#include <vector>

ColorScheme::ColorScheme(std::vector<double> p1, std::vector<std::vector<int>> p2) {
    m_scheme = p2;
    if (p1.empty()) {
        m_start_color_ranges = create_splitted_linear_distribution(p2.size(), FieldStats());
    } else {
        m_start_color_ranges = p1;
    }
}
std::vector<std::vector<int>> &ColorScheme::get_colors() {
    return m_scheme;
}
std::vector<double> &ColorScheme::get_ranges() {
    return m_start_color_ranges;
}

unsigned long ColorScheme::size() {
    return m_scheme.size();
}
void ColorScheme::save(cereal::XMLOutputArchive &archive) const {
    archive(m_scheme, m_start_color_ranges);
}
void ColorScheme::load(cereal::XMLInputArchive &archive) {
    archive(m_scheme, m_start_color_ranges);
}
QColor ColorScheme::get_color_from_value(double p_value) {
    unsigned long color_index = 0;
   
    while (color_index < (m_scheme.size() - 1) &&
           m_start_color_ranges[color_index] < p_value) {
        color_index++;
    }
    return QColor(m_scheme[color_index][0], m_scheme[color_index][1], m_scheme[color_index][2]);
}
