#include "PolygonItem.h"

#include <QGraphicsSceneMouseEvent>
#include <QLineEdit>
#include <QtGui>

#include <iostream>
PolygonItem::PolygonItem(QPolygon &polygon,unsigned p_id ,double p_value, double p_lon, double p_lat) : m_selected(false) {
    setPolygon(polygon);

    setData(ID, p_id);
    setData(LON, p_lon);
    setData(LAT, p_lat);
    set_value(p_value);// always after setting lon/lat since the tooltip needs to know these
    setPen(QPen(QColor(0, 0, 0), 0));
}

extern double *_field;

void PolygonItem::mousePressEvent(QGraphicsSceneMouseEvent *event) {

    if (event->buttons() == Qt::RightButton) {
  
        if (m_selected == true) {
            emit poly_context_menu_open(this);
        } else {
            emit poly_item_all_selections_canceled();
        }
    }
    if (event->buttons() == Qt::LeftButton) {
        if (m_selected == false) {
                setPen(QPen(QColor(0, 255, 0), 20));
                emit poly_item_was_selected(this);
                m_selected = true;
            setZValue(100);
        } else {
            emit poly_item_selection_canceled(this);
        }
    }
}

void PolygonItem::set_value(double p_value)
{
    setData(HEIGHT, p_value);
    setToolTip("ID:"+ data(ID).toString() +" Pos: (" + QString::number(data(LON).toDouble(),'f',2) +" / " + QString::number(data(LAT).toDouble(),'f',2)+ ") Value: " + data(HEIGHT).toString());

}

void PolygonItem::set_color(QColor p_color){

    setBrush(QBrush(p_color));
}

void PolygonItem::cancel_selection() {
    // qDebug("focusOutEvent");
    setPen(QPen(QColor(0, 0, 0), 0));
    setZValue(0);
    m_selected = false;
}

