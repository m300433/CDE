contains(QT_CONFIG, opengl):QT += opengl

NCROOT   = $$(NCROOT)

INCLUDEPATH += $$NCROOT/include

LIBS += -Wl,-rpath,$$NCROOT/lib -L$$NCROOT/lib -lnetcdf

TEMPLATE	= app
LANGUAGE	= C++

#CONFIG         += qt warn_on release debug
CONFIG         += qt warn_on release
DEFINES        += HAVE_LIBNETCDF HAVE_NETCDF4 QT_NO_DEBUG_OUTPUT QT_PRINTSUPPORT_LIB 
QMAKE_CFLAGS   +=  -g
QMAKE_CXXFLAGS +=  -g  -std=c++11
LIBS           +=  -g
QT              += printsupport

HEADERS		= qcustomplot.h MainWindow.h PolygonItem.h ConfigDialog.h ColorButton.h \
                GraphicsView.h View.h cdi.h grid.h colorSchemeMap.h colorScheme.h \
                color_distribution_functions.h color_config_window.h usage_window.h

SOURCES		= MainWindow.cpp PolygonItem.cpp ConfigDialog.cpp ColorButton.cpp \
		        GraphicsView.cpp View.cpp cdilib.c grid.c main.cpp qcustomplot.cpp \
                colorSchemeMap.cpp colorScheme.cpp color_distribution_functions.cpp \ 
                color_config_window.cpp usage_window.cpp

RESOURCES       = cde.qrc
