#include "View.h"
#include "cdi.h"

#include <QtGui>
#ifndef QT_NO_OPENGL
#include <QtOpenGL>
#endif
#include <QPushButton>
#include <QtPrintSupport/QPrintDialog>
#include <QtPrintSupport/QPrinter>

#include <iostream>
#include <math.h>
void View::create_graphics_view()
{
    std::string variable_name = "test";
    if(m_graphics_view_map.find(variable_name) == m_graphics_view_map.end())
    {
        m_graphics_view_map[variable_name] = std::make_shared<GraphicsView>();
        m_graphics_view_map[variable_name]->setRenderHint(QPainter::Antialiasing, false);
        // m_graphics_view_map[variable_name]->setDragMode(QGraphicsView::RubberBandDrag);
        // m_graphics_view_map[variable_name]->setDragMode(QGraphicsView::ScrollHandDrag);
        // m_graphics_view_map[variable_name]->setInteractive(true);
        m_graphics_view_map[variable_name]->setViewportUpdateMode(QGraphicsView::SmartViewportUpdate);
        // m_graphics_view_map[variable_name]->setViewportUpdateMode(QGraphicsView::NoViewportUpdate);
        // m_graphics_view_map[variable_name]->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        // m_graphics_view_map[variable_name]->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        m_graphics_view_map[variable_name]->scale(1.0, 1.0);
        m_graphics_view_map[variable_name]->setMinimumSize(600, 400);
        m_graphics_view_map[variable_name]->setCacheMode(QGraphicsView::CacheBackground);
        m_graphics_view_map[variable_name]->setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
        m_graphics_view_map[variable_name]->setResizeAnchor(QGraphicsView::AnchorUnderMouse);
        m_graphics_view_map[variable_name]->setBackgroundBrush(Qt::lightGray);

    }
}

void View::set_IDs(int p_vlist_id)
{
    m_vlist_id = p_vlist_id;
    m_variable_count = vlistNvars(m_vlist_id);
    init_variable_selection();
}

void View::init_variable_selection()
{
    char name[20];
    for(int i = 0; i < m_variable_count; i++)
    {
        vlistInqVarName(m_vlist_id, i, name);
        m_variable_selection->addItem(name);
    }
}
View::View(const QString &name, QWidget *parent) : QFrame(parent) {
    setFrameStyle(Sunken | StyledPanel);
    create_graphics_view();
    /*
    graphicsView = new GraphicsView;
    graphicsView->setRenderHint(QPainter::Antialiasing, false);
    // graphicsView->setDragMode(QGraphicsView::RubberBandDrag);
    // graphicsView->setDragMode(QGraphicsView::ScrollHandDrag);
    // graphicsView->setInteractive(true);
    graphicsView->setViewportUpdateMode(QGraphicsView::SmartViewportUpdate);
    // graphicsView->setViewportUpdateMode(QGraphicsView::NoViewportUpdate);
    // graphicsView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    // graphicsView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    graphicsView->scale(1.0, 1.0);
    graphicsView->setMinimumSize(600, 400);
    graphicsView->setCacheMode(QGraphicsView::CacheBackground);
    graphicsView->setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
    graphicsView->setResizeAnchor(QGraphicsView::AnchorUnderMouse);
    graphicsView->setBackgroundBrush(Qt::lightGray);
*/
    zoomSlider = new QSlider;
    zoomSlider->setMinimum(20);
    zoomSlider->setMaximum(400);
    zoomSlider->setValue(20);
    zoomSlider->setTickPosition(QSlider::TicksRight);

    // Zoom slider layout
    QVBoxLayout *zoomSliderLayout = new QVBoxLayout;
    zoomSliderLayout->addWidget(zoomSlider);

    rotateSlider = new QSlider;
    rotateSlider->setOrientation(Qt::Horizontal);
    rotateSlider->setMinimum(-360);
    rotateSlider->setMaximum(360);
    rotateSlider->setValue(0);
    rotateSlider->setTickPosition(QSlider::TicksBelow);

    // Rotate slider layout
    QHBoxLayout *rotateSliderLayout = new QHBoxLayout;
    rotateSliderLayout->addWidget(rotateSlider);

    QSize iconSize(8, 8);

    // Label layout

    QVBoxLayout *mainLayout = new QVBoxLayout();
    {
        QGridLayout* grid = new QGridLayout();
        if(m_graphics_view_map.find("test") == m_graphics_view_map.end())
        {
            std::cout << "ThERES AN ERROR: EXIT"<< std::endl;
            exit(EXIT_FAILURE);
        }
        grid->addWidget(m_graphics_view_map["test"].get());
        mainLayout->addLayout(grid, 1);


        QHBoxLayout *bottomLayout = new QHBoxLayout();
        {
            m_variable_selection = new QComboBox();
            QHBoxLayout *edit_layout = new QHBoxLayout();
            {
                edit_layout->setAlignment(Qt::AlignLeft);
                apply_range_button = new QPushButton(this);
                {
                    apply_range_button->setDefault(true);
                    apply_range_button->setText("Apply");
                    connect(apply_range_button, &QPushButton::released, this,
                            &View::emit_range_change);
                }

                min_value_edit = new QLineEdit(this);
                {
                    min_value_edit->setSizePolicy(QSizePolicy::Minimum,
                                                  min_value_edit->sizePolicy().horizontalPolicy());
                    min_value_edit->setValidator(new QDoubleValidator());
                    connect(min_value_edit, &QLineEdit::returnPressed, apply_range_button,
                            &QPushButton::released);
                }
                QLabel *min_value_edit_label = new QLabel("Min Value:");
                {
                    min_value_edit_label->setSizePolicy(
                        QSizePolicy::Minimum,
                        min_value_edit_label->sizePolicy().horizontalPolicy());
                }
                max_value_edit = new QLineEdit(this);
                {
                    max_value_edit->setSizePolicy(QSizePolicy::Minimum,
                                                  max_value_edit->sizePolicy().horizontalPolicy());
                    max_value_edit->setValidator(new QDoubleValidator());
                    connect(max_value_edit, &QLineEdit::returnPressed, apply_range_button,
                            &QPushButton::released);
                }

                QLabel *max_value_edit_label = new QLabel("Max Value:");
                {
                    max_value_edit_label->setSizePolicy(
                        QSizePolicy::Minimum,
                        max_value_edit_label->sizePolicy().horizontalPolicy());
                }

                edit_layout->addWidget(min_value_edit_label);
                edit_layout->addWidget(min_value_edit);
                edit_layout->addWidget(max_value_edit_label);
                edit_layout->addWidget(max_value_edit);
                edit_layout->addWidget(apply_range_button);
            }
            QHBoxLayout *variable_selection_layout = new QHBoxLayout();
            {
                variable_selection_layout->setAlignment(Qt::AlignRight);
                m_variable_selection = new QComboBox();

                variable_selection_layout->addWidget(m_variable_selection);
            }
            //TODO: MOVE THIS INTO A WINDOW OPEND WITH STRG+J
            /*
            QHBoxLayout *jump_lon_lat_layout = new QHBoxLayout();
            {
                QLineEdit *lat_edit = new QLineEdit();
                {}
                QLabel *lat_edit_label = new QLabel();
                { lat_edit_label->setText("Lat:"); }
                QLineEdit *lon_edit = new QLineEdit();
                {}
                QLabel *lon_edit_label = new QLabel();
                { lon_edit_label->setText("Lon:"); }
                QPushButton *jump_to_button = new QPushButton();

                jump_lon_lat_layout->addWidget(lat_edit_label);
                jump_lon_lat_layout->addWidget(lat_edit);
                jump_lon_lat_layout->addWidget(lon_edit_label);
                jump_lon_lat_layout->addWidget(lon_edit);
                jump_lon_lat_layout->addWidget(jump_to_button);
            }
            */
            bottomLayout->addLayout(edit_layout);
            bottomLayout->addLayout(variable_selection_layout);
        }
        mainLayout->addLayout(bottomLayout);
    }

    // topLayout->addLayout(naviLayout, 2, 0);

    // topLayout->addWidget(graphicsView, 0, 0);
    setLayout(mainLayout);

}

void View::setResetButtonEnabled() {
}

void View::setOpenGL(bool on) {
#ifndef QT_NO_OPENGL
    for(auto graphicsViewIterator : m_graphics_view_map){
    graphicsViewIterator.second->setViewport(on ? new QGLWidget(QGLFormat(QGL::SampleBuffers)) : new QWidget);}
#endif
}

void View::setAntialiasing(bool on) {
    for(auto graphicsViewIterator : m_graphics_view_map){
    graphicsViewIterator.second->setRenderHint(QPainter::Antialiasing, on);
}
}

void View::print() {
    /*
    QPrinter printer;
    QPrintDialog dialog(&printer, this);

    if ( dialog.exec() == QDialog::Accepted )
      {
        QPainter painter(&printer);
        graphicsView->render(&painter);
      }
    */
}

void View::zoomIn() {
    
    for(auto graphicsViewIterator : m_graphics_view_map){
    graphicsViewIterator.second->scaleView(pow((double)2, 0.5));
    }
}

void View::zoomOut() {

    for(auto graphicsViewIterator : m_graphics_view_map){
    graphicsViewIterator.second->scaleView(pow((double)2, -0.5));
    }
}



void View::emit_range_change() {
    double min_value = min_value_edit->text().toDouble();
    double max_value = max_value_edit->text().toDouble();
    emit range_updated(min_value, max_value);
}
void View::set_start_range(double p_min, double p_max)
{
    min_value_edit->setText(QString::number(p_min));
    max_value_edit->setText(QString::number(p_max));
}

GraphicsView *View::view() 
{
    GraphicsView *ptr = m_graphics_view_map["test"].get();
    return ptr;
}
