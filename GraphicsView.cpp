#include "GraphicsView.h"

#include <QDebug>
#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QRubberBand>
#include <QWheelEvent>

#include "PolygonItem.h"
#include <iostream>
#include <math.h>

GraphicsView::GraphicsView() {
    setCacheMode(CacheBackground);
    setRenderHint(QPainter::Antialiasing);

    m_current_scale = 1.0;
    scale(1.0, 1.0);
    setMinimumSize(600, 400);
    m_selection_rectangle = new QRubberBand(QRubberBand::Rectangle, this);
}
/*
void GraphicsView::drawBackground(QPainter *painter, const QRectF &rect)
{
    Q_UNUSED(rect);

    // Shadow
    QRectF sceneRect = this->sceneRect();
    qDebug() << "GraphicsView::drawBackground" << sceneRect;
    qDebug() << "GraphicsView::drawBackground" << this->mapToScene(this->viewport()->rect());

  //  setBackgroundBrush(Qt::blue);
    painter->setBrush(Qt::green);
    painter->setPen(Qt::black);
    painter->drawRect(sceneRect);
}
*/
void GraphicsView::drawForeground(QPainter *painter, const QRectF &rect) {
    Q_UNUSED(rect);

    // Shadow
    QRectF sceneRect = this->sceneRect();
    qDebug() << "GraphicsView::drawForeground" << sceneRect;
    qDebug() << "GraphicsView::drawForeground" << this->mapToScene(this->viewport()->rect());

    painter->setBrush(Qt::NoBrush);
    painter->setPen(QColor(255, 0, 0));
    painter->drawRect(sceneRect);
    painter->drawRect(sceneRect.left() - 5, sceneRect.bottom() - 5, sceneRect.width() + 10,
                      sceneRect.height() + 10);
}

void GraphicsView::scaleView(qreal scaleFactor) {
    //    qreal factor = matrix().scale(scaleFactor, scaleFactor).mapRect(QRectF(0, 0, 1,
    //    1)).width();
    // qreal factor = matrix().scale(scaleFactor, scaleFactor).mapRect(QRectF(0, 0, 100,
    // 100)).width();
    qreal factor = matrix().scale(scaleFactor, scaleFactor).mapRect(QRectF(0, 0, 100, 100)).width();

    // std::cout  << factor << std::endl;
    qDebug() << "scaleView: " << scaleFactor << factor;
    qDebug() << "scaleView: " << scaleFactor << factor;

    if (factor < 1.0 || factor > 1000)
        return;

    // m_current_scale *= scaleFactor;
    // std::cout << "SF:" <<  scaleFactor << std::endl;
    scale(scaleFactor, scaleFactor);
}

void GraphicsView::xresizeEvent(QResizeEvent *event) {
    qDebug("GraphicsView::resizeEvent");
    //  fitInView(sceneRect(), Qt::KeepAspectRatioByExpanding);
    fitInView(sceneRect(), Qt::KeepAspectRatio);
    std::cout << "asdasd" << std::endl;

    QAbstractScrollArea::resizeEvent(event);
}
/*
void GraphicsView::mousePressEvent(QMouseEvent *event) {
    if (event->buttons() == Qt::LeftButton && event->modifiers() == Qt::ControlModifier) {
        m_zoom_rectangle_active = true;
        m_selection_rectangle_orgigin = event->pos();
        m_selection_rectangle->setGeometry(QRect(m_selection_rectangle_orgigin, QSize()));
        m_selection_rectangle->show();
    }
}
void GraphicsView::mouseReleaseEvent(QMouseEvent *event) {
    if ( m_zoom_rectangle_active == true) {
        m_selection_rectangle->hide();
        std::cout << "hid the rectangle" << std::endl;
        std::cout << m_selection_rectangle_orgigin.x() << " " << m_selection_rectangle_orgigin.y()
                  << std::endl;
        std::cout << event->pos().x() << " " << event->pos().y() << std::endl;
        QRectF lol = QRectF(mapToScene(m_selection_rectangle_orgigin), mapToScene(event->pos()));
        fitInView(lol, Qt::KeepAspectRatio);
        m_zoom_rectangle_active = false;
    }
}

void GraphicsView::mouseMoveEvent(QMouseEvent *event) {
    if (m_zoom_rectangle_active == true) {
        m_selection_rectangle->setGeometry(
            QRect(m_selection_rectangle_orgigin, event->pos()).normalized());
    }
}
*/
void GraphicsView::wheelEvent(QWheelEvent *event) {

    qDebug() << "wheelEvent: " << pow((double)2, -event->delta() / 240.0);
    const QPointF scene_pointe = mapToScene(event->pos());
    QGraphicsItem *under_the_mouse_item = scene()->itemAt(scene_pointe, QGraphicsView::transform());
    scaleView(pow((double)2, -event->delta() / 240.0));
    if (under_the_mouse_item && event->delta() < 0) {
        centerOn(under_the_mouse_item);
        QCursor::setPos(mapToGlobal(mapFromScene(under_the_mouse_item->boundingRect().center())));
    }

    qDebug() << "GraphicsView::wheelEvent scene " << this->sceneRect();
    qDebug() << "GraphicsView::wheelEvent viewport " << this->viewport()->rect();
    qDebug() << "GraphicsView::wheelEvent map " << this->mapToScene(QPoint(0, 0));
    qDebug() << "GraphicsView::wheelEvent map " << this->mapToScene(this->viewport()->rect());
}
void GraphicsView::resetView(){
    QPointF center = mapToScene(QPoint(viewport()->rect().width(),0));
    centerOn(center);
    //emit view_resetted();
}

    //QPointF center = graphicsView->mapToScene(QPoint(graphicsView->viewport()->rect().width(), 0));
    //						   graphicsView->viewport()->rect().height()/2));
