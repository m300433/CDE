#ifndef MAINWINDOW_H
#define MAINWINDOW_H

//#include <QtGui>
#include <QMainWindow>

#include "ConfigDialog.h"
#include "PolygonItem.h"
#include "View.h"
#include "colorSchemeMap.h"
#include "color_config_window.h"
#include "color_distribution_functions.h"
#include "fieldStats.h"
#include "math.h"
#include "qcustomplot.h"
#include "usage_window.h"

#include <stack>
#include <vector>

#define RAD2DEG (180. / M_PI) /* conversion for rad to deg */

class QAction;
class QActionGroup;
class QListWidget;
class QMenu;
class QErrorMessage;
class QGraphicsScene;
class QGraphicsView;
class QSplitter;
class QLineEdit;
class MainWindow : public QMainWindow {
    Q_OBJECT

  public:
    MainWindow();
    ~MainWindow();

    void open(const QString &fn);
    void openDialog();

    QColor workspaceColor;
    QColor panelsColor;
    QString appStyle, workingDir;
    bool autoSave;
    QString appLanguage;

  protected:
    void closeEvent(QCloseEvent *event);

  public slots:
    // void open( const QString& filename );
    //    void openFile();
    //    void closeFile();
    void saveDialog();

    // Settings
    void showPreferencesDialog();

    void readSettings();
    void saveSettings();
    void applyUserSettings();
    void changeAppStyle(const QString &s);
    void setAppColors(const QColor &wc, const QColor &pc);
    void poly_item_was_selected(PolygonItem *p_item);
    void poly_context_menu_open(PolygonItem *p_item);
    void handle_range_update(double p_new_min_value, double p_new_max_value);

  private slots:
    //    void save();

    void cancel_all_selections();
    void cancel_selection(PolygonItem *p_item);
    void zoomIn();
    void zoomOut();
    void print();
    void toggleOpenGL();
    void toggleAntialiasing();

    void about();
    void open_edit_dialog(PolygonItem *p_item, int p_edit_all);
    void toggleColorOptions();
    void toggleUsageWindow();

  signals:
    void new_file_opend();

  private:
    void init();

    char *m_cdi_filename;

    void createActions();
    void createMenus();
    void createToolBars();
    void createStatusBar();
    std::vector<int> ccols;
    QDockWidget *mainDock;

    ColorSchemeMap m_color_scheme_map;
    View *view;
    QGraphicsScene *scene;
    std::stack<PolygonItem *> m_last_changes; //REF: MOVE TO GRAPHICS VIEw
    std::vector<PolygonItem *> m_selected_polys; // REF: stays

    QErrorMessage *openErrorMessageDialog;

    QMenu *fileMenu;

    QInputDialog *edit_dialog;

    QToolBar *fileToolBar;
    QToolBar *editToolBar;
    QToolBar *zoomToolBar;
    QToolBar *configToolBar;

    ColorConfigWindow *m_color_config_window;
    UsageWindow* m_usage_window;

    QAction *saveAct;
    QAction *printAct;
    QAction *quitAct;
    QAction *undoAct;
    QAction *actionShowPreferencesDialog;
    QAction *antialiasAct;
    QAction *openglAct;
    QAction *zoomOutAct;
    QAction *zoomInAct;
    QAction *aboutAct;
    QAction *setAllAct;
    QAction *setAct;
    QAction *toggleColorSliderAct;
    QAction *toggleUsage;
    QCustomPlot *colorPlot;

    int streamID;
    int vlistID;
    int vlistID2;
    int gridID;
    int zaxisID;
    int levelID;
    int taxisID;

    int vdate;
    int vtime;

    QString *varName, *varLongname, *varStdname, *varUnits;

    // simulation data parameters
    int filetype;
    int gridtype;
    int ncorner;
    int nlevel;
    int grid_is_circular;
    int *grid_mask;
    double level;
    double *m_grid_center_lat, *m_grid_center_lon;
    double *grid_corner_lat, *grid_corner_lon;
    double *field;

    void initScene(void);
    void initGrid(void);
    void initField(void);
    void readField(void);
    int writeField(const char *filename);
    void createColorConfigWindow();
    void createUsageWindow();
    void update_colors();
    // Settings
    std::string m_color_scheme_name;
    // Color stuff TODO: better comment
    std::unique_ptr<ColorSchemeMap> m_new_color_scheme_map;
    ColorScheme m_color_scheme;
    std::function<std::vector<double>(ColorScheme &, FieldStats)> m_fp_color_distribution;
    FieldStats m_field_stats;
};

#endif
