#ifndef COLOR_SCHEME_H
#define COLOR_SCHEME_H

#include "cereal/archives/xml.hpp"
#include "cereal/types/memory.hpp"
#include "cereal/types/vector.hpp"
#include <QColor>
#include <map>
#include <memory>
#include <vector>
class ColorScheme {
  private:
    std::vector<double> m_color_change_rates;
    std::vector<double> m_start_color_ranges;
    std::vector<std::vector<int>> m_scheme;

  public:
    ColorScheme() {
    }
    ColorScheme(std::vector<double> p1, std::vector<std::vector<int>> p2);
    std::vector<std::vector<int>> &get_colors();
    std::vector<double> &get_ranges();
    void save(cereal::XMLOutputArchive &archive) const;
    void load(cereal::XMLInputArchive &archive);
    unsigned long size();
    QColor get_color_from_value(double p_value);
};
#endif
