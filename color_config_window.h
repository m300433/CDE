#ifndef COLORCONFIG_H
#define COLORCONFIG_H

#include "colorScheme.h"
#include "colorSchemeMap.h"
#include "fieldStats.h"

#include "qcustomplot.h"

#include <QComboBox>
#include <QWidget>

#include <iostream>
#include <map>
#include <vector>

class ColorConfigWindow : public QWidget {
    Q_OBJECT
  private:
    QHBoxLayout *m_mainLayout;
    QVBoxLayout *m_colorManipulationLayout;
    QHBoxLayout *m_colorSliderLayout;

    QWidget *m_colorPlotWidget;

    QCustomPlot *m_colorPlot;
    QCPColorMap *m_colorMap;
    QCPColorGradient *m_gradient;
    QCPColorScale *m_colorScale;

    QComboBox *m_colorSchemeChooser;
    QSlider *m_upperBoundsSlider;
    QSlider *m_lowerBoundsSlider;

    ColorSchemeMap &m_color_schemes;
    FieldStats &m_field_stats;

    float m_current_min;
    float m_current_max;

    QVector<double> m_y_axis_values;

    void create_layout();
    void update_plot();
    void update_slider();
    void update_color_map();
    void update_color_scheme_chooser();
    void update_gradient();
    void update_color_scale();
    void set_ranges(double min, double max);

    float get_current_range();
  public slots:
    void update();

  public:
    ColorConfigWindow(ColorSchemeMap &p_color_schemes, FieldStats &p_field_stats,
                      QWidget *p_parent = NULL);
};
#endif
