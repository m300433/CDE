#include "colorSchemeMap.h"
#include "cereal/archives/xml.hpp"
#include "cereal/types/map.hpp"
#include "cereal/types/string.hpp"
#include "cereal/types/vector.hpp"
#include "colorDefinitions.h"
#include "colorScheme.h"
#include <fstream>

ColorSchemeMap::ColorSchemeMap() {
    //TEMPORARY: loading of xml not implemented or the loading function is not used yet (19.4.2017)
    m_color_map["dirty_rainbow"] = ColorScheme(std::vector<double>(), dirty_rainbow);
    m_color_map["gray_scale_16"] = ColorScheme(std::vector<double>(), gray_scale_16);
    m_color_map["gray_scale_8"] = ColorScheme(std::vector<double>(), gray_scale_8);
    m_color_map["orography"] = ColorScheme(std::vector<double>(), orography);
    m_color_map["pastel2_rainbow_16"] = ColorScheme(std::vector<double>(), pastel2_rainbow_16);
    m_color_map["pastel2_rainbow_8"] = ColorScheme(std::vector<double>(), pastel2_rainbow_8);
    m_color_map["pastel2_rainbow_31"] = ColorScheme(std::vector<double>(), pastel2_rainbow_31);
    m_color_map["rainbow_16"] = ColorScheme(std::vector<double>(), rainbow_16);
    m_color_map["rainbow_8"] = ColorScheme(std::vector<double>(), rainbow_8);
}

void ColorSchemeMap::set_color_scheme(std::string p_color_scheme_name) {
    m_current_color_scheme_name = p_color_scheme_name;
}

void ColorSchemeMap::save_schemes() {
    //Unfinished feature: not in version 0.1;
    /*
    std::string color_scheme_file_name;
    std::ofstream out_stream;
    for (auto it : m_color_map) {
        color_scheme_file_name = "cde_color_schemes/" + it.first + ".xml";
        out_stream = std::ofstream(color_scheme_file_name);

        cereal::XMLOutputArchive out_archive(out_stream);
        std::cout << color_scheme_file_name << " " << it.second.size() << std::endl;
        out_archive(cereal::make_nvp(it.first, it.second));
    }
    */
}

ColorScheme &ColorSchemeMap::current_scheme() {
    return m_color_map[m_current_color_scheme_name];
}
std::string ColorSchemeMap::current_scheme_name() {
    return m_current_color_scheme_name;
}
std::vector<double> &ColorSchemeMap::current_scheme_ranges() {
    return m_color_map[m_current_color_scheme_name].get_ranges();
}
unsigned long ColorSchemeMap::current_scheme_size() {
    return m_color_map[m_current_color_scheme_name].size();
}
std::vector<std::vector<int>> &ColorSchemeMap::current_scheme_colors() {
    return m_color_map[m_current_color_scheme_name].get_colors();
}
