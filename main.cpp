#include <QApplication>

#include "MainWindow.h"


void usage()
{
  fprintf(stderr, "usage: cde  [filename]\n");
  exit(-1);
}


int main(int argc, char ** argv)
{
  QApplication app(argc, argv);

  if ( argc > 2 ) usage();

  MainWindow *mainwindow = new MainWindow();

  if ( argc == 2 )
    mainwindow->open(argv[1]);
  else
    mainwindow->openDialog();
 
  mainwindow->show();

  app.connect(&app, SIGNAL(lastWindowClosed()), &app, SLOT(quit()));

  return app.exec();
}
