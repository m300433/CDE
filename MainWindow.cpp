#include <QtWidgets>
#ifndef QT_NO_OPENGL
#include <QtOpenGL>
#endif

#include <string.h>

#include "MainWindow.h"
#include "PolygonItem.h"
#include "View.h"
#include "cdi.h"
#include "color_config_window.h"
#include "color_distribution_functions.h"
#include "grid.h"

#include <iostream>
#include <memory>
#include <stack>
#include <vector>

int _editMode = 0;
double _editValue;
double *_field;

MainWindow::MainWindow() {
    init();

    //  fprintf(stderr, "device depth:        %d\n", this->depth());
    //  fprintf(stderr, "device height:       %d\n", this->height());
    //  fprintf(stderr, "device width:        %d\n", this->width());
    //  fprintf(stderr, "device heightMM:     %d\n", this->heightMM());
    //  fprintf(stderr, "device widthMM:      %d\n", this->widthMM());
    //  fprintf(stderr, "device logicalDpiX:  %d\n", this->logicalDpiX());
    //  fprintf(stderr, "device logicalDpiY:  %d\n", this->logicalDpiY());
    //  fprintf(stderr, "device physicalDpiX: %d\n", this->physicalDpiX());
    //  fprintf(stderr, "device physicalDpiY: %d\n", this->physicalDpiY());
    //  fprintf(stderr, "device numColors:    %d\n", this->numColors());
    setAttribute(Qt::WA_DeleteOnClose);

    resize(1200, 800);
    // fprintf(stderr, "device height:       %d\n", this->height());
    // fprintf(stderr, "device width:        %d\n", this->width());
}

void MainWindow::init() {
    appStyle = qApp->style()->objectName();
    setWindowTitle(tr("Climate Data Editor"));

    m_color_scheme_map = ColorSchemeMap();
    m_color_scheme_map.save_schemes();
    m_color_scheme_map.set_color_scheme("orography");
    std::vector<double> color_distribution = {-18000, -7000, -6000, -5000, -4000, -3000,
                                              -2000,  -1000, 0,     200,   400,   600,
                                              800,    1000,  1500,  2000,  99000};

    resize(800, 600);
    /*
    m_color_scheme = std::map<double, std::vector<int>>({{-18000, {0, 5, 25}},
                                                      {-7000, {0, 10, 50}},
                                                      {-6000, {0, 80, 125}},
                                                      {-5000, {0, 150, 200}},
                                                      {-4000, {86, 197, 184}},
                                                      {-3000, {172, 245, 168}},
                                                      {-2000, {211, 250, 211}},
                                                      {-1000, {250, 255, 255}},
                                                      {0, {70, 120, 50}},
                                                      {200, {120, 100, 50}},
                                                      {400, {146, 126, 60}},
                                                      {600, {198, 178, 80}},
                                                      {800, {250, 230, 100}},
                                                      {1000, {250, 234, 126}},
                                                      {1500, {252, 238, 152}},
                                                      {2000, {252, 243, 177}},
                                                      {99000, {253, 249, 216}}});

    */
    m_color_scheme = m_color_scheme_map.current_scheme();
    openErrorMessageDialog = new QErrorMessage(this);

    createActions();
    createMenus();
    createToolBars();
    createStatusBar();
    createColorConfigWindow();
    createUsageWindow();

    readSettings();
    applyUserSettings();

    view = new View("Title");
    connect(view, &View::range_updated, this, &MainWindow::handle_range_update);
    setCentralWidget(view);
    view->setAttribute(Qt::WA_Hover, true);
}

MainWindow::~MainWindow() {
    saveSettings();
}
void MainWindow::initScene() {

    scene = new QGraphicsScene;
    // scene->setSceneRect(-18000, -9000, 36000, 18000);
    // scene->setItemIndexMethod(QGraphicsScene::NoIndex);
    // scene->setBspTreeDepth (5);
    // printf("bspTreeDepth %d\n", scene->bspTreeDepth());

    QPolygon polygon(ncorner + 1);
    // QGraphicsPolygonItem *item;
    double scale = 100;

    double val;
    int k;
    double lon_bounds[ncorner + 1], lat_bounds[ncorner + 1];
    double lon, lat;

    _field = field;

    for (unsigned i = 0; i < m_field_stats.gridsize; i++) {
        val = field[i];
        lon = m_grid_center_lon[i];
        lat = m_grid_center_lat[i];

        for (k = 0; k < ncorner; ++k) {
            lon_bounds[k] = grid_corner_lon[i * ncorner + k];
            lat_bounds[k] = grid_corner_lat[i * ncorner + k];
        }

        for (k = 0; k < ncorner; ++k) {
            if ((lon - lon_bounds[k]) > 270)
                lon_bounds[k] += 360;
            if ((lon_bounds[k] - lon) > 270)
                lon_bounds[k] -= 360;
        }

        // std::string  coors;
        for (k = 0; k < ncorner; k++) {
            polygon.setPoint(k, (int)(scale * lon_bounds[k]), (int)(-1 * scale * lat_bounds[k]));
            // coors += k + " " + std::to_string(scale * lon_bounds[k]) + " " + std::to_string(-1 *
            // scale * lat_bounds[k]) + "\n";
        }
        // std::cout << coors << std::endl;
        polygon.setPoint(k, (int)(scale * lon_bounds[0]), (int)(-1 * scale * lat_bounds[0]));

        PolygonItem *item = new PolygonItem(polygon,i, val, lon, lat);
        //      item->setSelected(true);
        // item->setFlag(QGraphicsItem::ItemIsSelectable, true);
        item->setFlag(QGraphicsItem::ItemIsFocusable, true);
        connect(item, SIGNAL(poly_item_was_selected(PolygonItem *)), this,
                SLOT(poly_item_was_selected(PolygonItem *)));
        connect(item, SIGNAL(poly_item_all_selections_canceled()), this,
                SLOT(cancel_all_selections()));
        connect(item, SIGNAL(poly_context_menu_open(PolygonItem *)), this,
                SLOT(poly_context_menu_open(PolygonItem *)));
        connect(item, SIGNAL(poly_item_selection_canceled(PolygonItem *)), this,
                SLOT(cancel_selection(PolygonItem *)));

        scene->addItem(item);
    }
    update_colors();
}

void MainWindow::print() {
    view->print();

    statusBar()->showMessage(tr("Ready"), 2000);
}

void MainWindow::about() {
    QMessageBox::about(this, tr("About CDEDIT"), tr("Climate Data Editor"));
}

void MainWindow::createActions() {
    saveAct = new QAction(QIcon(":/images/save.png"), tr("&Save..."), this);
    saveAct->setShortcut(tr("Ctrl+S"));
    saveAct->setStatusTip(tr("Save"));
    connect(saveAct, SIGNAL(triggered()), this, SLOT(saveDialog()));

    printAct = new QAction(QIcon(":/images/print.png"), tr("&Print..."), this);
    printAct->setShortcut(tr("Ctrl+P"));
    printAct->setStatusTip(tr("Print"));
    connect(printAct, SIGNAL(triggered()), this, SLOT(print()));

    quitAct = new QAction(QIcon(":/images/cancel.png"), tr("&Quit"), this);
    quitAct->setShortcut(tr("Ctrl+Q"));
    quitAct->setStatusTip(tr("Quit the application"));
    connect(quitAct, SIGNAL(triggered()), this, SLOT(close()));

    actionShowPreferencesDialog = new QAction(tr("&Preferences..."), this);
    // connect(actionShowPreferencesDialog, SIGNAL(activated()), this,
    // SLOT(showPreferencesDialog()));

    toggleUsage = new QAction(tr("&Usage"), this);
    toggleUsage->setCheckable(true);
    toggleUsage->setChecked(false);
    connect(toggleUsage, &QAction::triggered, this, &MainWindow::toggleUsageWindow);

    toggleColorSliderAct = new QAction(tr("&Color options"), this);
    toggleColorSliderAct->setCheckable(true);
    toggleColorSliderAct->setChecked(false);
    connect(toggleColorSliderAct, SIGNAL(triggered()), this, SLOT(toggleColorOptions()));

    aboutAct = new QAction(tr("&About"), this);
    aboutAct->setStatusTip(tr("Show the application's About box"));
    connect(aboutAct, SIGNAL(triggered()), this, SLOT(about()));

    zoomInAct = new QAction(QIcon(":/images/zoomin.png"), tr("Zoom &In"), this);
    zoomInAct->setShortcut(tr("Ctrl++"));
    connect(zoomInAct, SIGNAL(triggered()), this, SLOT(zoomIn()));

    zoomOutAct = new QAction(QIcon(":/images/zoomout.png"), tr("Zoom &Out"), this);
    zoomOutAct->setShortcut(tr("Ctrl+-"));
    zoomOutAct->setEnabled(true);
    connect(zoomOutAct, SIGNAL(triggered()), this, SLOT(zoomOut()));

    antialiasAct = new QAction(tr("&Antialiasing"), this);
    antialiasAct->setCheckable(true);
    antialiasAct->setChecked(false);
    connect(antialiasAct, SIGNAL(triggered()), this, SLOT(toggleAntialiasing()));

    openglAct = new QAction(tr("&OpenGL"), this);
    openglAct->setCheckable(true);
    openglAct->setChecked(false);
#ifndef QT_NO_OPENGL
    openglAct->setEnabled(QGLFormat::hasOpenGL());
#else
    openglAct->setEnabled(false);
#endif
    connect(openglAct, SIGNAL(triggered()), this, SLOT(toggleOpenGL()));
}

void MainWindow::toggleAntialiasing() {
    view->setAntialiasing(antialiasAct->isChecked());

    if (antialiasAct->isChecked())
        statusBar()->showMessage(tr("Antialiasing on"), 2000);
    else
        statusBar()->showMessage(tr("Antialiasing off"), 2000);
}

void MainWindow::toggleOpenGL() {
    view->setOpenGL(openglAct->isChecked());

    if (openglAct->isChecked())
        statusBar()->showMessage(tr("OpenGL on"), 2000);
    else
        statusBar()->showMessage(tr("OpenGL off"), 2000);
}

void MainWindow::zoomIn() {
    emit view->zoomIn();
}

void MainWindow::zoomOut() {
    emit view->zoomOut();
}

void MainWindow::closeEvent(QCloseEvent *event) {
}

void MainWindow::createMenus() {
    fileMenu = menuBar()->addMenu(tr("&File"));
    fileMenu->addAction(saveAct);
    fileMenu->addSeparator();
    fileMenu->addAction(printAct);
    fileMenu->addSeparator();
    fileMenu->addAction(quitAct);

}

void MainWindow::createToolBars() {
    fileToolBar = addToolBar(tr("File"));
    fileToolBar->addAction(saveAct);
    fileToolBar->addAction(printAct);

    zoomToolBar = addToolBar(tr("Zoom"));
    zoomToolBar->addAction(zoomInAct);
    zoomToolBar->addAction(zoomOutAct);

    configToolBar = addToolBar(tr("Configurations"));
    // configToolBar->addAction(toggleColorSliderAct);
    // configToolBar->addAction(toggleUsage);
}

void MainWindow::createUsageWindow() {
    m_usage_window = new UsageWindow();
}

void MainWindow::createColorConfigWindow() {
    m_color_config_window = new ColorConfigWindow(m_color_scheme_map, m_field_stats);
    connect(this, SIGNAL(new_file_opend()), m_color_config_window, SLOT(update()));
}
void MainWindow::toggleColorOptions() {
    m_color_config_window->setVisible(!m_color_config_window->isVisible());
}

void MainWindow::toggleUsageWindow() {
    m_usage_window->setVisible(!m_usage_window->isVisible());
}
void MainWindow::createStatusBar() {
    statusBar()->showMessage(tr("Ready"));
}

void MainWindow::showPreferencesDialog() {
    ConfigDialog *cd = new ConfigDialog(this);
    cd->setAttribute(Qt::WA_DeleteOnClose);
    // cd->setColumnSeparator(columnSeparator);
    cd->exec();
}

void MainWindow::open(const QString &fn) {
    QByteArray afilename = fn.toLatin1();
    const char *fname = afilename.constData();

    streamID = streamOpenRead(fname);
    if (streamID < 0) {
        openErrorMessageDialog->showMessage(QString("Open failed on '%1'").arg(m_cdi_filename));
        // quit application !!!
        statusBar()->showMessage(QString("Open failed on '%1'").arg(m_cdi_filename));
    } else {
        filetype = streamInqFiletype(streamID);
        m_cdi_filename = strdup(fname);
        qDebug("Open file: %s", fname);

        initField();
        readField();

        initScene();

        // view = new View("Top left view");
        view->view()->setScene(scene);
        view->view()->fitInView(view->view()->sceneRect(), Qt::KeepAspectRatio);

        emit new_file_opend();
        m_color_scheme.get_ranges() = create_splitted_linear_distribution(m_color_scheme.size(), m_field_stats);
        update_colors();
        view->set_start_range(m_field_stats.min, m_field_stats.max);
        // setCentralWidget(view);
    }
}

void MainWindow::openDialog() {
    QString fn = QFileDialog::getOpenFileName(this, "Climate Data Editor -- Open data file", "",
                                              "Cimate Data files (*.grb *.nc *.srv *.ext *ieg);;"
                                              "GRIB files (*.grb);;"
                                              "netCDF files (*.nc);;"
                                              "SERVICE files (*.srv);;"
                                              "EXTRA files (*.ext);;"
                                              "IEG files (*.ieg);;"
                                              "All files(*)");

    if (!fn.isEmpty())
        open(fn);
    else {
        // openErrorMessageDialog->showMessage(QString("No file selected!"));
        // statusBar()->showMessage(tr("Loading aborted"), 2000);
        fprintf(stderr, "No file selected!\n");
        exit(-1);
    }
}

void MainWindow::saveDialog() {
    qDebug() << "saveDialog: " << m_cdi_filename;
    QString fn =
        QFileDialog::getSaveFileName(this, "Climate Data Editor -- Open data file", m_cdi_filename,
                                     "Cimate Data files (*.grb *.nc *.srv *.ext *ieg);;"
                                     "GRIB files (*.grb);;"
                                     "netCDF files (*.nc);;"
                                     "SERVICE files (*.srv);;"
                                     "EXTRA files (*.ext);;"
                                     "IEG files (*.ieg);;"
                                     "All files(*)");

    if (!fn.isEmpty()) {
        int status;
        qDebug() << "Save file: " << fn;
        status = writeField(fn.toLatin1().constData());

        if (status != 0)
            statusBar()->showMessage(tr("Save file error!"), 2000);
    } else {
        statusBar()->showMessage(tr("Save file aborted"), 2000);
    }
}
struct vlist_t;
void MainWindow::initField(void) {
    char buffer[65536];
    int varID = 0;

    vlistID = streamInqVlist(streamID);

    std::cout << vlistNvars(vlistID) << std::endl; 
     char name[100];
    for(int i = 0; i < vlistNvars(vlistID); i++)
    {
        vlistInqVarName(vlistID, i, name);
        printf("%s\n", name);
    }
    vlistClearFlag(vlistID);
    vlistDefFlag(vlistID, 0, 0, 1);
    vlistID2 = vlistCreate();
    vlistCopyFlag(vlistID2, vlistID);

    taxisID = vlistInqTaxis(vlistID);

    buffer[0] = 0;
    vlistInqVarName(vlistID, varID, buffer);
    varName = new QString(buffer);

    buffer[0] = 0;
    vlistInqVarLongname(vlistID, varID, buffer);
    if (buffer[0])
        varLongname = new QString(buffer);
    else
        varLongname = NULL;

    buffer[0] = 0;
    vlistInqVarStdname(vlistID, varID, buffer);
    if (buffer[0])
        varStdname = new QString(buffer);
    else
        varStdname = NULL;

    buffer[0] = 0;
    vlistInqVarUnits(vlistID, varID, buffer);
    if (buffer[0])
        varUnits = new QString(buffer);
    else
        varUnits = NULL;

    m_field_stats.missval = vlistInqVarMissval(vlistID, varID);

    gridID = vlistInqVarGrid(vlistID, varID);
    m_field_stats.gridsize = gridInqSize(gridID);
    zaxisID = vlistInqVarZaxis(vlistID, varID);
    nlevel = zaxisInqSize(zaxisID);

    gridtype = gridInqType(gridID);

    qDebug() << "varName: " << varName->toLatin1();
    qDebug("gridsize: %d", m_field_stats.gridsize);

    initGrid();
    view->set_IDs(vlistID);
}

void MainWindow::initGrid(void) {
    char units[128];
    int nlon, nlat;
    int nalloc;
    int i;
    int lgrid_gen_bounds = 0;

    if (gridInqType(gridID) != GRID_LONLAT && gridInqType(gridID) != GRID_GAUSSIAN &&
        gridInqType(gridID) != GRID_GME && gridInqType(gridID) != GRID_CURVILINEAR &&
        gridInqType(gridID) != GRID_UNSTRUCTURED)
        qFatal("Output of %s data failed!", gridNamePtr(gridInqType(gridID)));

    if (gridInqType(gridID) != GRID_UNSTRUCTURED && gridInqType(gridID) != GRID_CURVILINEAR) {
        if (gridInqType(gridID) == GRID_GME) {
            gridID = gridToUnstructured(gridID);
            grid_mask = (int *)malloc(m_field_stats.gridsize * sizeof(int));
            gridInqMask(gridID, grid_mask);
        } else {
            gridID = gridToCurvilinear(gridID);
            lgrid_gen_bounds = 1;
        }
    }
    /*
    nlon     = gridInqXsize(gridID);
    nlat     = gridInqYsize(gridID);

    qDebug("nlon = %d   nlat = %d", nlon, nlat);
    */
    if (gridInqType(gridID) == GRID_UNSTRUCTURED)
        ncorner = gridInqNvertex(gridID);
    else
        ncorner = 4;

    qDebug("ncorner: %d", ncorner);

    grid_is_circular = gridIsCircular(gridID);

    m_grid_center_lat = (double *)malloc(m_field_stats.gridsize * sizeof(double));
    m_grid_center_lon = (double *)malloc(m_field_stats.gridsize * sizeof(double));

    gridInqYvals(gridID, m_grid_center_lat);
    gridInqXvals(gridID, m_grid_center_lon);

    /* Convert lat/lon units if required */

    gridInqYunits(gridID, units);

    if (strncmp(units, "radian", 6) == 0) {
        for (i = 0; i < m_field_stats.gridsize; i++) {
            m_grid_center_lat[i] *= RAD2DEG;
            m_grid_center_lon[i] *= RAD2DEG;
        }
    } else if (strncmp(units, "degrees", 7) == 0) {
        /* No conversion necessary */
    } else {
        qDebug("Unknown units (%s) supplied for grid1 center lat/lon: "
               "proceeding assuming degrees",
               units);
    }

    //  if ( luse_grid_corner )
    {
        if (ncorner == 0)
            qFatal("grid corner missing!");
        nalloc = ncorner * m_field_stats.gridsize;
        grid_corner_lat = (double *)malloc(nalloc * sizeof(double));
        grid_corner_lon = (double *)malloc(nalloc * sizeof(double));

        if (gridInqYbounds(gridID, NULL) && gridInqXbounds(gridID, NULL)) {
            gridInqYbounds(gridID, grid_corner_lat);
            gridInqXbounds(gridID, grid_corner_lon);
        } else {
            /*
            if ( lgrid_gen_bounds )
              {
                genXbounds(nlon, nlat, m_grid_center_lon, grid_corner_lon);
                genYbounds(nlon, nlat, m_grid_center_lat, grid_corner_lat);
              }
            else
            */
            qFatal("Grid corner missing!");
        }

        if (strncmp(units, "radian", 6) == 0) {
            /* Note: using units from latitude instead from bounds */
            for (i = 0; i < ncorner * m_field_stats.gridsize; i++) {
                grid_corner_lat[i] *= RAD2DEG;
                grid_corner_lon[i] *= RAD2DEG;
            }
        } else if (strncmp(units, "degrees", 7) == 0) {
            /* No conversion necessary */
        } else {
            qDebug("Unknown units (%s) supplied for grid1 center lat/lon: "
                   "proceeding assuming degrees",
                   units);
        }
    }
}

#include <math.h>

#ifndef DBL_IS_EQUAL
/*
#define  DBL_IS_EQUAL(x,y) (fabs(x - y) <= 2.0*(y*DBL_EPSILON + DBL_MIN))
*/
#define DBL_IS_EQUAL(x, y) (!(fabs(x - y) > 0))
#endif

void fieldStat(double *p_field, FieldStats &p_field_stats) {
    int i, ivals, imiss = 0;
    double arrmin, arrmax, arrmean, arrvar;

    if (p_field_stats.nmiss > 0) {
        ivals = 0;
        arrmean = 0;
        arrvar = 0;
        arrmin = 1e50;
        arrmax = -1e50;
        for (i = 0; i < p_field_stats.gridsize; i++) {
            if (!DBL_IS_EQUAL(p_field[i], p_field_stats.missval)) {
                if (p_field[i] < arrmin)
                    arrmin = p_field[i];
                if (p_field[i] > arrmax)
                    arrmax = p_field[i];
                arrmean += p_field[i];
                arrvar += p_field[i] * p_field[i];
                ivals++;
            }
        }
        imiss = p_field_stats.gridsize - ivals;
        p_field_stats.gridsize = ivals;
    } else {
        arrmean = p_field[0];
        arrvar = p_field[0];
        arrmin = p_field[0];
        arrmax = p_field[0];
        for (i = 1; i < p_field_stats.gridsize; i++) {
            if (p_field[i] < arrmin)
                arrmin = p_field[i];
            if (p_field[i] > arrmax)
                arrmax = p_field[i];
            arrmean += p_field[i];
            arrvar += p_field[i] * p_field[i];
        }
    }

    if (p_field_stats.gridsize) {
        arrmean = arrmean / p_field_stats.gridsize;
        arrvar = arrvar / p_field_stats.gridsize - arrmean * arrmean;
    }

    if (imiss != p_field_stats.nmiss && p_field_stats.nmiss > 0)
        fprintf(stdout, "Found %d of %d missing values!\n", imiss, p_field_stats.nmiss);

    p_field_stats.min = arrmin;
    p_field_stats.max = arrmax;
    p_field_stats.mean = arrmean;
    p_field_stats.var = arrvar;
    p_field_stats.range = arrmax - arrmin;
}

void MainWindow::readField() {
    int varID = 0, levelID = 0, tsID = 0;

    qDebug() << "readField: ";

    int nrecs;
    size_t size;

    nrecs = streamInqTimestep(streamID, tsID);
    if (nrecs == 0)
        qFatal("Reading of timestep %d failed!\n", tsID);

    vdate = taxisInqVdate(taxisID);
    vtime = taxisInqVtime(taxisID);

    size = m_field_stats.gridsize * sizeof(double);

    field = (double *)malloc(size);

    if (field == NULL)
        qFatal("No memory!");

    // field is set in this function (among others?)
    streamReadVarSlice(streamID, varID, levelID, field, &m_field_stats.nmiss);

    // sets m_field_stats
    fieldStat(field, m_field_stats);

    qDebug() << "fieldStat: " << m_field_stats.min << m_field_stats.max << m_field_stats.mean
             << m_field_stats.var << (m_field_stats.max - m_field_stats.min)
             << (m_field_stats.max - m_field_stats.min) / m_field_stats.var;
}

int MainWindow::writeField(const char *filename) {
    int streamID;
    int varID = 0, levelID = 0, tsID = 0;
    int status = 0;
    int nmiss;

    streamID = streamOpenWrite(filename, filetype);
    if (streamID < 0) {
        fprintf(stderr, "%s\n", cdiStringError(streamID));
        return (1);
    } else {
        streamDefVlist(streamID, vlistID2);

        taxisDefVdate(taxisID, vdate);
        taxisDefVtime(taxisID, vtime);

        qDebug() << "writeField: ";
        streamDefTimestep(streamID, tsID);

        nmiss = 0;
        for (int i = 0; i < m_field_stats.gridsize; ++i)
            if (DBL_IS_EQUAL(field[i], m_field_stats.missval))
                nmiss++;

        streamWriteVar(streamID, varID, field, nmiss);

        streamClose(streamID);
    }

    return (status);
}

void MainWindow::changeAppStyle(const QString &s) {
    // style keys are case insensitive
    if (appStyle.toLower() == s.toLower())
        return;

    qApp->setStyle(s);
    appStyle = qApp->style()->objectName();

    QPalette pal = qApp->palette();
    pal.setColor(QPalette::Active, QPalette::Base, QColor(panelsColor));
    qApp->setPalette(pal);
}

void MainWindow::setAppColors(const QColor &wc, const QColor &pc) {
    if (workspaceColor != wc) {
        workspaceColor = wc;
        // workspace->setBackground(QBrush(wc));
    }

    if (panelsColor == pc)
        return;

    panelsColor = pc;
    QPalette pal = qApp->palette();
    pal.setColor(QPalette::Active, QPalette::Base, QColor(panelsColor));
    qApp->setPalette(pal);
}

void MainWindow::applyUserSettings() {
    // workspace->setBackground(QBrush(workspaceColor));

    QPalette pal = qApp->palette();
    pal.setColor(QPalette::Active, QPalette::Base, QColor(panelsColor));
    qApp->setPalette(pal);
}

void MainWindow::readSettings() {
#ifdef Q_OS_MAC // Mac
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, "cdedit", "cdedit");
#else
    QSettings settings(QSettings::NativeFormat, QSettings::UserScope, "cdedit", "cdedit");
#endif

    /* ---------------- group General --------------- */
    settings.beginGroup("/General");
    //  show_windows_policy = (ShowWindowsPolicy)settings.value("/ShowWindowsPolicy",
    //  MainWindow::ActiveFolder).toInt();

    changeAppStyle(settings.value("/Style", appStyle).toString());
    autoSave = settings.value("/AutoSave", true).toBool();

    settings.beginGroup("/Colors");
    workspaceColor = settings.value("/Workspace", "darkGray").value<QColor>();
    // see http://doc.trolltech.com/4.2/qvariant.html for instructions on qcolor <-> qvariant
    // conversion
    panelsColor = settings.value("/Panels", "#ffffff").value<QColor>();
    settings.endGroup(); // Colors

    settings.beginGroup("/Paths");
    workingDir = settings.value("/WorkingDir", qApp->applicationDirPath()).toString();
    settings.endGroup(); // Paths
    settings.endGroup();
    /* ------------- end group General --------------- */
}

void MainWindow::saveSettings() {
#ifdef Q_OS_MAC // Mac
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, "cdedit", "cdedit");
#else
    QSettings settings(QSettings::NativeFormat, QSettings::UserScope, "cdedit", "cdedit");
#endif

    /* ---------------- group General --------------- */
    settings.beginGroup("/General");
    //  settings.setValue("/ShowWindowsPolicy", show_windows_policy);
    settings.setValue("/Style", appStyle);
    settings.setValue("/AutoSave", autoSave);

    settings.beginGroup("/Colors");
    settings.setValue("/Workspace", workspaceColor);
    settings.setValue("/Panels", panelsColor);
    settings.endGroup(); // Colors

    settings.beginGroup("/Paths");
    settings.setValue("/WorkingDir", workingDir);
    settings.endGroup(); // Paths
    settings.endGroup();
    /* ---------------- end group General ------------ */
}

void undo() {
}

void redo() {
}
void MainWindow::cancel_all_selections() {
    for (auto item : m_selected_polys) {
        item->cancel_selection();
    }
    m_selected_polys.clear();
}
void MainWindow::cancel_selection(PolygonItem *p_item) {
    for (unsigned long i = 0; i < m_selected_polys.size(); i++) {
        if (m_selected_polys[i] == p_item) {
            m_selected_polys.erase(m_selected_polys.begin() + i);
            p_item->cancel_selection();
            return;
        }
    }
}
void MainWindow::poly_item_was_selected(PolygonItem *p_item) {
    m_selected_polys.push_back(p_item);
}
void MainWindow::open_edit_dialog(PolygonItem *p_item, int edit_all) {
    std::string window_title = "Edit single value";
    if (edit_all > 0) {
        window_title = "Edit all selected values";
    }
    bool success;
    float result;
    float initial_value;
    if (edit_all == 1) {
        initial_value = 0.0;
    } else {
        initial_value = p_item->data(HEIGHT).toDouble();
    }
    result =
        QInputDialog::getDouble(this, tr(window_title.c_str()), tr("New Value:"), initial_value,
                                -2323232323, std::numeric_limits<double>::max(), 15, &success,
                                this->windowFlags() & ~Qt::WindowContextHelpButtonHint);
    if (success) {
        if (edit_all == 1) {
            for (auto item : m_selected_polys) {
                item->set_value(result);
                item->set_color(m_color_scheme.get_color_from_value(result));
                field[item->data(ID).toInt()] = result;
            }
        } else {
            p_item->set_value(result);
            p_item->set_color(m_color_scheme.get_color_from_value(result));
            field[p_item->data(ID).toInt()] = result;
        }
    }
}
void MainWindow::poly_context_menu_open(PolygonItem *p_item) {
    setAct = new QAction(tr("&Edit Value"), this);
    connect(setAct, &QAction::triggered, [=]() { open_edit_dialog(p_item, 0); });

    setAllAct = new QAction(tr("&EditAllSelectedValues"), this);
    connect(setAllAct, &QAction::triggered, [=]() { open_edit_dialog(p_item, 1); });

    QMenu menu(this);
    menu.addAction(setAct);
    menu.addAction(setAllAct);
    menu.exec(QCursor::pos());
}
void MainWindow::update_colors() {
    for (auto item : scene->items()) {
        ((PolygonItem *)item)
            ->set_color(m_color_scheme.get_color_from_value(item->data(HEIGHT).toDouble()));
    }
}

void MainWindow::handle_range_update(double p_new_min_value, double p_new_max_value) {
    FieldStats temp_stats_for_new_range;
    temp_stats_for_new_range.min = p_new_min_value;
    temp_stats_for_new_range.max = p_new_max_value;
    m_color_scheme.get_ranges() =
        create_splitted_linear_distribution(m_color_scheme.size(), temp_stats_for_new_range);
    update_colors();
}
