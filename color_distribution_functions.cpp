#include "colorScheme.h"
#include "fieldStats.h"
#include <iostream>
#include <vector>
std::vector<double> create_linear_distribution(unsigned long p_cnt_colors,
                                               FieldStats p_data_limits) {
    std::vector<double> color_distribution;
    double step = p_data_limits.range / (float)p_cnt_colors;
    for (double i = p_data_limits.min; i < 0; i += step) {
        color_distribution.push_back(i);
    }
    return color_distribution;
}

std::vector<double> create_splitted_linear_distribution(unsigned long p_cnt_colors,
                                                        FieldStats p_data_limits) {
    p_cnt_colors -= 2;
    std::vector<double> color_distribution;
    double cnt_sub_zero = std::ceil(p_cnt_colors/2);
    double cnt_above_zero = std::floor(p_cnt_colors/2);
    double step_sub_zero = p_data_limits.min / cnt_sub_zero;
    double step_above_zero = p_data_limits.max / cnt_above_zero;
    for (int i = cnt_sub_zero  ; i > 0; i--) {
        color_distribution.push_back(step_sub_zero * i);
    }
    color_distribution.push_back(0);
    for (int i = 1; i < cnt_above_zero + 1; i++) {
        color_distribution.push_back(step_above_zero * i);
    }
    color_distribution.push_back(p_data_limits.max*2);
    return color_distribution;
}
