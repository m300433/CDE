#ifndef VIEW_H
#define VIEW_H

#include "colorSchemeMap.h"

#include <QFrame>
#include <QComboBox>

#include "GraphicsView.h"
#include "colorScheme.h"
#include <QLineEdit>
#include <QPushButton>

#include <memory>

class QGraphicsView;
class QLabel;
class QSlider;
class QToolButton;

class View : public QFrame {
    Q_OBJECT
  public:
    View(const QString &name, QWidget *parent = 0);

    GraphicsView *view() ;
    void setOpenGL(bool on);
    void setAntialiasing(bool on);

    void scaleView(qreal scaleFactor);
    void set_IDs(int p_vlist_id);//TEMP FOR REFACTOR, if still present=> sorry

signals:
    void range_updated(double min_value, double max_value);
  public slots:
    void zoomIn();
    void zoomOut();
    void print();
    void set_start_range(double p_min, double p_max);

  private slots:
    void setResetButtonEnabled();
    void emit_range_change();
    void create_graphics_view();

  private:
    //map of string = variable_name and the view for it
    std::map<std::string, std::shared_ptr<GraphicsView>> m_graphics_view_map; // REF: to map
    QLabel *label;
    QToolButton *resetButton;

    QSlider *zoomSlider;
    QSlider *rotateSlider;

    QPushButton *apply_range_button; //Stays
    QLineEdit *min_value_edit; //Stays
    QLineEdit *max_value_edit; //Stays
    QComboBox *m_variable_selection;
    int m_variable_count;
    int m_vlist_id;
    void init_variable_selection();
};

#endif
