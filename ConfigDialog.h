#ifndef ConfigDialog_H
#define ConfigDialog_H

#include <QDialog>
#include <QCheckBox>

class QGroupBox;
class QPushButton;
class QTabWidget;
class QStackedWidget;
class QWidget;
class QComboBox;
class QSpinBox;
class QLabel;
class QRadioButton;
class QListWidget;
class ColorButton;

//! Preferences dialog
class ConfigDialog : public QDialog
{
    Q_OBJECT

public:
    ConfigDialog( QWidget* parent, Qt::WindowFlags fl = 0 );

public slots:
    virtual void languageChange();

    void accept();
    void apply();

    void setCurrentPage(int index);
    //   void setColumnSeparator(const QString& sep);

    //application
    //void pickApplicationFont();
    void pickPanelsColor();
    void pickWorkspaceColor();

private:
    void initPlotsPage();
    void initAppPage();

    QTabWidget *plotsTabWidget, *appTabWidget;
    QPushButton *buttonOk, *buttonCancel, *buttonApply;
    QStackedWidget * generalDialog;
    QWidget *appColors;
    QWidget *application;
    QComboBox *boxStyle;
    ColorButton *btnWorkspace, *btnPanels;
    QListWidget * itemsList;
    QLabel *labelFrameWidth, *lblLanguage, *lblWorkspace, *lblPanels, *lblPageHeader;
    QLabel *lblStyle;
};

#endif // CONFIGDIALOG_H
